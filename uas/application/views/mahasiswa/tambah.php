<div class="container">

    <div class="row mt-3">
        <div class="col-md-6">

        <div class="card">
            <div class="card-header">
                Form Tambah Data Mahasiswa
            </div>
            <div class="card-body">
                <form action="" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama">
                    <div class="form-text text-danger"><?= form_error('nama'); ?></div>
                </div>
                <br>
                <div class="form-group">
                    <label for="nim">NIM</label>
                    <input type="number" name="nim" class="form-control" id="nim">
                    <div class="form-text text-danger"><?= form_error('nim'); ?></div>
                </div>
                <br>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control" id="email">
                    <div class="form-text text-danger"><?= form_error('email'); ?></div>
                </div>
                <br>
                    <div class="form-group">
                        <label for="jurusan">Jurusan</label>
                        <select class="form-control"
                        id="jurusan" name="jurusan">
                            <option value="D3 Akuntansi">D3 Akuntansi</option>
                            <option value="S1 Sistem Informasi">S1 Sistem Informasi</option>
                            <option value="S1 Hukum">S1 Hukum</option>
                            <option value="S1 Teknik Sipil">S1 Teknik Sipil</option>
                        </select>
                    </div>
                    <br>
                    <button type="submit" name="tambah" class="btn btn-primary
                    float-right">Tambah Data</button>
                </form>
            </div>
        </div>

            
        </div>
    </div>