<div class="container">
    <?php if( $this->session->flashdata('flash') ) : ?>
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                Data Mahasiswa<strong> berhasil!</strong> <?= $this->session->flashdata('flash'); ?>.
                <button type="button" class="close" data-bs-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <div class="row mt-3"> 
        <div class="col-md-6">
            <a href="<?= base_url(); ?>mahasiswa/tambah" class="btn btn-primary">Tambah Data Mahasiswa</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-6">
            <form action="" method="post">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Cari data mahasiswa.." name="keyword">
                <button class="btn btn-outline-primary" type="submit">Cari</button>
              </div>
            </form>
        </div>
    </div>

        <div class="row mt-3">
            <div class="col-md-6">
                <h3>Datar Mahasiswa</h3>
                <?php if( empty($mahasiswa) ) : ?>
                    <div class="alert alert-danger" role="alert">
                    Data Tidak Ditemukan!
                    </div>
                <?php endif; ?>
                <ul class="list-group">
                    <?php foreach( $mahasiswa as $mhs ) : ?>
                        <li class="list-group-item">
                            <?= $mhs['nama']; ?>
                            <a href="<?= base_url(); ?>mahasiswa/hapus/<?= $mhs['id']; ?>" class="badge bg-danger float-right" onclick="return confirm('yakin?');">hapus</a>
                            <a href="<?= base_url(); ?>mahasiswa/ubah/<?= $mhs['id']; ?>" class="badge bg-success float-right">ubah</a>
                            <a href="<?= base_url(); ?>mahasiswa/detail/<?= $mhs['id']; ?>" class="badge bg-primary float-right">detail</a>
                        </li>
                    <?php endforeach; ?>
                </ul>
             </div>
        </div>
        <br>
        <br>

                <?php 

                $url = file_get_contents('https://api.kawalcorona.com/indonesia');
                $data = json_decode($url, true);

                ?>
            <div class="container">
                <h4 class="text-uppercase mb-4">INFO COVID-19 INDONESIA</h4>
            
            <div class="container">
            <div class="row">
                <div class="col-md-4">
                <div class="bg-primary box text-white">
                    <div class="row">
                    <div class="col-md-6">
                        <h5>Positif</h5>
                        <h2><?php  echo $data[0] ['positif'] ?></h2>
                        <h5>Jiwa</h5>
                    </div>
                    
                    </div>
                </div>
                </div>

                <div class="col-md-4">
                <div class="bg-success box text-white">
                    <div class="row">
                    <div class="col-md-6">
                        <h5>Sembuh</h5>
                        <h2><?php  echo $data[0] ['sembuh'] ?></h2>
                        <h5>Jiwa</h5>
                    </div>
                    </div>
                </div>
                </div>

                <div class="col-md-4">
                <div class="bg-danger box text-white">
                    <div class="row">
                    <div class="col-md-6">
                        <h5>Meninggal</h5>
                        <h2><?php  echo $data[0] ['meninggal'] ?></h2>
                        <h5>Jiwa</h5>
                    </div>
                    </div>
                </div>
                </div>
                </div>
            </div>

</div>